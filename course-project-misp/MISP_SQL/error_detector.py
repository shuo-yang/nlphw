# Error detector
from .utils import semantic_unit_segment, np


class ErrorDetector:
    """
    This is the class for Error Detector.
    """
    def __init__(self):
        return

    def detection(self, tag_seq, start_pos=0, bool_return_first=False, *args, **kwargs):
        """
        Error detection.
        :param tag_seq: a sequence of semantic units.
        :param start_pos: the starting pointer to examine.
        :param bool_return_first: Set to True to return the first error only.
        :return: a list of pairs of (erroneous semantic unit, its position in tag_seq).
        """
        raise NotImplementedError


class ErrorDetectorSim(ErrorDetector):
    """
    This is a simulated error detector which always detects the exact wrong decisions.
    """
    def __init__(self):
        ErrorDetector.__init__(self)

    def detection(self, tag_seq, start_pos=0, bool_return_first=False, eval_tf=None, *args, **kwargs):
        if start_pos >= len(tag_seq):
            return []

        semantic_units, pointers = semantic_unit_segment(tag_seq)
        err_su_pointer_pairs = []
        for semantic_unit, pointer in zip(semantic_units, pointers):
            if pointer < start_pos:
                continue

            bool_correct = eval_tf[pointer]
            if not bool_correct:
                err_su_pointer_pairs.append((semantic_unit, pointer))
                if bool_return_first:
                    return err_su_pointer_pairs

        return err_su_pointer_pairs


class ErrorDetectorProbability(ErrorDetector):
    """
    This is the probability-based error detector.
    """
    def __init__(self, threshold):
        """
        Constructor of the probability-based error detector.
        :param threshold: A float number; the probability threshold.
        """
        ErrorDetector.__init__(self)
        self.prob_threshold = threshold
        self.invoke_cnts = 0

    def detection(self, tag_seq, start_pos=0, bool_return_first=False, *args, **kwargs):
        self.invoke_cnts += 1
        if start_pos >= len(tag_seq):
            return []

        semantic_units, pointers = semantic_unit_segment(tag_seq)
        err_su_pointer_pairs = []
        for semantic_unit, pointer in zip(semantic_units, pointers):
            if pointer < start_pos:
                continue

            prob = semantic_unit[-2]
            # if the decision's probability is lower than the threshold, consider it as an error
            if prob < self.prob_threshold:
                err_su_pointer_pairs.append((semantic_unit, pointer))
                if bool_return_first:
                    return err_su_pointer_pairs

        return err_su_pointer_pairs


class ErrorDetectorBayesDropout(ErrorDetector):
    """
    This is the Bayesian Dropout-based error detector.
    """
    def __init__(self, threshold):
        """
        Constructor of the Bayesian Dropout-based error detector.
        :param threshold: A float number; the standard deviation threshold.
        """
        ErrorDetector.__init__(self)
        self.stddev_threshold = threshold

    def detection(self, tag_seq, start_pos=0, bool_return_first=False, *args, **kwargs):
        if start_pos >= len(tag_seq):
            return []

        semantic_units, pointers = semantic_unit_segment(tag_seq)
        err_su_pointer_pairs = []
        for semantic_unit, pointer in zip(semantic_units, pointers):
            if pointer < start_pos:
                continue

            # if the decision's stddev is greater than the threshold, consider it as an error
            stddev = np.std(semantic_unit[-2])
            if stddev > self.stddev_threshold:
                err_su_pointer_pairs.append((semantic_unit, pointer))
                if bool_return_first:
                    return err_su_pointer_pairs

        return err_su_pointer_pairs


import torch
from torch.distributions import Categorical

class ErrorDetectorNet(ErrorDetector):
    """
    This is the probability-based error detector.
    """
    def __init__(self, max_tag_len, net: torch.nn.Module, train_policy=False):
        """
        Constructor of the probability-based error detector.
        :param threshold: A float number; the probability threshold.
        """
        ErrorDetector.__init__(self)
        self.max_tag_len = max_tag_len
        self.net = net
        self.clips = []
        self.states = []
        self.actions = []
        self.rewards = []
        self.train_policy = train_policy
        self.invoke_cnts = 0

    def detection(self, tag_seq, start_pos=0, bool_return_first=False, *args, **kwargs):
        self.invoke_cnts += 1
        if start_pos >= len(tag_seq):
            return []

        # the first entry of the error net indicate not output error
        semantic_pointer_pairs = [(tag_seq[ind], ind) for ind in range(min(self.max_tag_len - 1, len(tag_seq))) if tag_seq[ind][0] != "O" ]
        semantic_pointer_pairs = [item for item in semantic_pointer_pairs if item[1] >= start_pos]

        if len(semantic_pointer_pairs) == 0:
            return []

        inp_feat = [semantic_pointer_pairs[ind][0][-2] if ind < len(semantic_pointer_pairs) else 0.0 for ind in range(self.max_tag_len)]
        inp_feat = torch.tensor(inp_feat)[None][None]
        out_score = self.net(inp_feat)[0][0]
        out = torch.nn.functional.softmax(out_score[:(len(semantic_pointer_pairs) + 1)], dim=0)
        distri = Categorical(out)
        out_ind = distri.sample()

        if self.train_policy:
            self.clips.append((len(semantic_pointer_pairs) + 1))
            self.states.append(inp_feat)
            self.actions.append(out_ind)
            self.rewards.append(-1.0) 
        # (state, action, reward) each time invoke of error detection will introduces a negative reward
        out_ind = out_ind.item()
        if out_ind == 0:
            return []
        else:
            return [semantic_pointer_pairs[out_ind - 1]]





if __name__ == '__main__':
    # Some work for testing
    net = torch.nn.Sequential(
        torch.nn.Linear(2, 128),
        torch.nn.ReLU(),
        torch.nn.Linear(128, 2),
        torch.nn.ReLU())

    print(net(torch.randn(2)))



