# models.py

from optimizers import *
from nerdata import *
from utils import *

from collections import Counter
from typing import List

import numpy as np

# from concurrent.futures import ThreadPoolExecutor

class ProbabilisticSequenceScorer(object):
    """
    Scoring function for sequence models based on conditional probabilities.
    Scores are provided for three potentials in the model: initial scores (applied to the first tag),
    emissions, and transitions. Note that CRFs typically don't use potentials of the first type.

    Attributes:
        tag_indexer: Indexer mapping BIO tags to indices. Useful for dynamic programming
        word_indexer: Indexer mapping words to indices in the emission probabilities matrix
        init_log_probs: [num_tags]-length array containing initial sequence log probabilities
        transition_log_probs: [num_tags, num_tags] matrix containing transition log probabilities (prev, curr)
        emission_log_probs: [num_tags, num_words] matrix containing emission log probabilities (tag, word)
    """
    def __init__(self, tag_indexer: Indexer, word_indexer: Indexer, init_log_probs: np.ndarray, transition_log_probs: np.ndarray, emission_log_probs: np.ndarray):
        self.tag_indexer = tag_indexer
        self.word_indexer = word_indexer
        self.init_log_probs = init_log_probs
        self.transition_log_probs = transition_log_probs
        self.emission_log_probs = emission_log_probs

    def score_init(self, sentence_tokens: List[Token], tag_idx: int):
        return self.init_log_probs[tag_idx]

    def score_transition(self, sentence_tokens: List[Token], prev_tag_idx: int, curr_tag_idx: int):
        return self.transition_log_probs[prev_tag_idx, curr_tag_idx]

    def score_emission(self, sentence_tokens: List[Token], tag_idx: int, word_posn: int):
        word = sentence_tokens[word_posn].word
        word_idx = self.word_indexer.index_of(word) if self.word_indexer.contains(word) else self.word_indexer.index_of("UNK")
        return self.emission_log_probs[tag_idx, word_idx]


class HmmNerModel(object):
    """
    HMM NER model for predicting tags

    Attributes:
        tag_indexer: Indexer mapping BIO tags to indices. Useful for dynamic programming
        word_indexer: Indexer mapping words to indices in the emission probabilities matrix
        init_log_probs: [num_tags]-length array containing initial sequence log probabilities
        transition_log_probs: [num_tags, num_tags] matrix containing transition log probabilities (prev, curr)
        emission_log_probs: [num_tags, num_words] matrix containing emission log probabilities (tag, word)
    """
    def __init__(self, tag_indexer: Indexer, word_indexer: Indexer, init_log_probs, transition_log_probs, emission_log_probs):
        self.tag_indexer = tag_indexer
        self.word_indexer = word_indexer
        self.init_log_probs = init_log_probs
        self.transition_log_probs = transition_log_probs
        self.emission_log_probs = emission_log_probs
        self.scorer = ProbabilisticSequenceScorer(tag_indexer, word_indexer, init_log_probs, transition_log_probs, emission_log_probs)

    def decode(self, sentence_tokens: List[Token]):
        """
        See BadNerModel for an example implementation
        :param sentence_tokens: List of the tokens in the sentence to tag
        :return: The LabeledSentence consisting of predictions over the sentence
        """
        sen_len = len(sentence_tokens)
        tag_len = len(self.init_log_probs)

        # For dynamic programming
        dp = np.ones((tag_len, sen_len)) * np.NINF
        
        backptr = np.zeros((tag_len, sen_len))

        for tag in range(tag_len):
            dp[tag, 0] = self.scorer.score_init(sentence_tokens, tag) + self.scorer.score_emission(sentence_tokens, tag, 0)

        for pos in range(1, sen_len):
            for tag in range(tag_len):
                values = [
                    dp[prev, pos -1] + self.scorer.score_transition(sentence_tokens, prev, tag) + self.scorer.score_emission(sentence_tokens, tag, pos)
                    for prev in range(0, tag_len)
                ]
                best_prev_tag = np.argmax(values)
                backptr[tag, pos] = best_prev_tag
                dp[tag, pos] = values[best_prev_tag]

        # Starts from the last token
        tag_idx = np.argmax(dp[:, -1])
        preds = ["" for i in range(sen_len)]
        preds[-1] = self.tag_indexer.get_object(tag_idx)
        
        for i in range(sen_len -2, -1, -1):
            tag_idx = backptr[int(tag_idx), i + 1]
            preds[i] = self.tag_indexer.get_object(tag_idx)

        results = LabeledSentence(sentence_tokens, chunks_from_bio_tag_seq(preds))
        return results


def train_hmm_model(sentences: List[LabeledSentence]) -> HmmNerModel:
    """
    Uses maximum-likelihood estimation to read an HMM off of a corpus of sentences.
    Any word that only appears once in the corpus is replaced with UNK. A small amount
    of additive smoothing is applied.
    :param sentences: training corpus of LabeledSentence objects
    :return: trained HmmNerModel
    """
    # Index words and tags. We do this in advance so we know how big our
    # matrices need to be.
    tag_indexer = Indexer()
    word_indexer = Indexer()
    word_indexer.add_and_get_index("UNK")
    word_counter = Counter()
    for sentence in sentences:
        for token in sentence.tokens:
            word_counter[token.word] += 1.0
    for sentence in sentences:
        for token in sentence.tokens:
            # If the word occurs fewer than two times, don't index it -- we'll treat it as UNK
            get_word_index(word_indexer, word_counter, token.word)
        for tag in sentence.get_bio_tags():
            tag_indexer.add_and_get_index(tag)
    # Count occurrences of initial tags, transitions, and emissions
    # Apply additive smoothing to avoid log(0) / infinities / etc.
    init_counts = np.ones((len(tag_indexer)), dtype=float) * 0.001
    transition_counts = np.ones((len(tag_indexer),len(tag_indexer)), dtype=float) * 0.001
    emission_counts = np.ones((len(tag_indexer),len(word_indexer)), dtype=float) * 0.001
    for sentence in sentences:
        bio_tags = sentence.get_bio_tags()
        for i in range(0, len(sentence)):
            tag_idx = tag_indexer.add_and_get_index(bio_tags[i])
            word_idx = get_word_index(word_indexer, word_counter, sentence.tokens[i].word)
            emission_counts[tag_idx][word_idx] += 1.0
            if i == 0:
                init_counts[tag_idx] += 1.0
            else:
                transition_counts[tag_indexer.add_and_get_index(bio_tags[i-1])][tag_idx] += 1.0
    # Turn counts into probabilities for initial tags, transitions, and emissions. All
    # probabilities are stored as log probabilities
    print(repr(init_counts))
    init_counts = np.log(init_counts / init_counts.sum())
    # transitions are stored as count[prev state][next state], so we sum over the second axis
    # and normalize by that to get the right conditional probabilities
    transition_counts = np.log(transition_counts / transition_counts.sum(axis=1)[:, np.newaxis])
    # similar to transitions
    emission_counts = np.log(emission_counts / emission_counts.sum(axis=1)[:, np.newaxis])
    print("Tag indexer: %s" % tag_indexer)
    print("Initial state log probabilities: %s" % init_counts)
    print("Transition log probabilities: %s" % transition_counts)
    print("Emission log probs too big to print...")
    print("Emission log probs for India: %s" % emission_counts[:,word_indexer.add_and_get_index("India")])
    print("Emission log probs for Phil: %s" % emission_counts[:,word_indexer.add_and_get_index("Phil")])
    print("   note that these distributions don't normalize because it's p(word|tag) that normalizes, not p(tag|word)")
    return HmmNerModel(tag_indexer, word_indexer, init_counts, transition_counts, emission_counts)


def get_word_index(word_indexer: Indexer, word_counter: Counter, word: str) -> int:
    """
    Retrieves a word's index based on its count. If the word occurs only once, treat it as an "UNK" token
    At test time, unknown words will be replaced by UNKs.
    :param word_indexer: Indexer mapping words to indices for HMM featurization
    :param word_counter: Counter containing word counts of training set
    :param word: string word
    :return: int of the word index
    """
    if word_counter[word] < 1.5:
        return word_indexer.add_and_get_index("UNK")
    else:
        return word_indexer.add_and_get_index(word)

class FeatureBasedSequenceScorer(object):
    """
    Feature-based sequence scoring model. Note that this scorer is instantiated *for every example*: it contains
    the feature cache used for that example.
    """
    def __init__(self, tag_indexer: Indexer, features: List[List[np.ndarray]], optimizer: Optimizer):
        self.tag_indexer = tag_indexer
        self.features = features
        self.optim = optimizer

    def score_init(self, sentence_tokens: List[Token], tag_idx: int):
        tag = self.tag_indexer.get_object(tag_idx)
        if isI(tag):
            return -9999
        else:
            return 0

    def score_transition(self, sentence_tokens: List[Token], prev_tag_idx: int, curr_tag_idx: int):
        curr_tag = self.tag_indexer.get_object(curr_tag_idx)
        curr_tag_label = get_tag_label(curr_tag)
        prev_tag = self.tag_indexer.get_object(prev_tag_idx)
        prev_tag_label = get_tag_label(prev_tag)
        if curr_tag_label != prev_tag_label and isI(curr_tag) and isI(prev_tag):
            return -9999
        if curr_tag_label != prev_tag_label and isI(curr_tag) and isB(prev_tag):
            return -9999
        if isO(prev_tag) and isI(curr_tag):
            return -9999
        return 0

    def score_emission(self, sentence_tokens: List[Token], tag_idx: int, word_posn: int):
        feature_val = self.features[word_posn][tag_idx]
        score_val = self.optim.score(feature_val)
        return score_val

class CrfNerModel(object):
    def __init__(self, tag_indexer: Indexer, feature_indexer: Indexer, optimizer: Optimizer, use_beam=False, beam_size=2):
        self.tag_indexer = tag_indexer
        self.feature_indexer = feature_indexer
        self.optim = optimizer
        self.use_beam = use_beam
        self.beam_size = beam_size

    def beam_dp(
        self, 
        sentence_tokens: List[Token], 
        dp: np.ndarray, 
        backptr: np.ndarray, 
        scorer: FeatureBasedSequenceScorer
    ) -> None:
        import heapq
        h = []

        for tag_idx in range(len(self.tag_indexer)):
            heapq.heappush(h, (dp[tag_idx][0], tag_idx))

        for pos in range(1, len(sentence_tokens)):
            top_k = heapq.nlargest(self.beam_size, h)
            for tag_idx in range(len(self.tag_indexer)):
                val = np.NINF
                for (prev_val, prev_t) in top_k:
                    tmp = prev_val + scorer.score_transition(sentence_tokens, prev_t, tag_idx)
                    if tmp > val:
                        val = tmp
                        backptr[tag_idx][pos] = prev_t
                dp[tag_idx][pos] = scorer.score_emission(sentence_tokens, tag_idx, pos) + val
            h.clear()
            for tag_idx in range(len(self.tag_indexer)):
                heapq.heappush(h, (dp[tag_idx][pos], tag_idx))

    def norm_dp(
        self,
        sentence_tokens: List[Token],
        dp: np.ndarray,
        backptr: np.ndarray,
        scorer: FeatureBasedSequenceScorer
    ) -> None:
        sen_len = len(sentence_tokens)
        tag_len = len(self.tag_indexer)
        for pos in range(1, sen_len):
            for tag_idx in range(tag_len):
                maxval = np.NINF
                for prev_t in range(tag_len):
                    val = dp[prev_t, pos - 1] + scorer.score_transition(sentence_tokens, prev_t, tag_idx)
                    if val > maxval:
                       maxval = val
                       backptr[tag_idx, pos] = prev_t
                dp[tag_idx, pos] = maxval + scorer.score_emission(sentence_tokens, tag_idx, pos)

    def decode(self, sentence_tokens: List[Token]) -> LabeledSentence:
        tag_len = len(self.tag_indexer)
        sen_len = len(sentence_tokens)
        features_eval = [[[] for k in range(0, tag_len)] for j in range(0, sen_len)]
        for word_ind in range(sen_len):
            for tag_ind in range(tag_len):
                tag_str = self.tag_indexer.get_object(tag_ind)
                features_eval[word_ind][tag_ind] = extract_emission_features(
                    sentence_tokens, word_ind, tag_str, self.feature_indexer, add_to_indexer=False)
        scorer = FeatureBasedSequenceScorer(self.tag_indexer, features_eval, self.optim)
        dp = np.zeros((tag_len, sen_len))
        backptr = np.zeros((tag_len, sen_len))
        for tag_ind in range(tag_len):
            dp[tag_ind, 0] = scorer.score_init(sentence_tokens, tag_ind)
            dp[tag_ind, 0] += scorer.score_emission(sentence_tokens, tag_ind, 0)
        if self.use_beam:
            self.beam_dp(sentence_tokens, dp, backptr, scorer)
        else:
            self.norm_dp(sentence_tokens, dp, backptr, scorer)
        pred_tags = ["" for i in range(0, sen_len)]
        curr_tag_ind =  np.argmax(dp[:, -1])
        pred_tags[-1] = self.tag_indexer.get_object(curr_tag_ind)
        for pos in range(sen_len -2, -1, -1):
            curr_tag_ind = backptr[int(curr_tag_ind), pos + 1]
            pred_tags[pos] = self.tag_indexer.get_object(curr_tag_ind)
        labeled_sentence = LabeledSentence(sentence_tokens, chunks_from_bio_tag_seq(pred_tags))
        return labeled_sentence

def modify_counter_vector(sparse_vector: Counter, indices: List, valb: float) -> None:
    for f_key in indices:
        sparse_vector[f_key] = sparse_vector[f_key] + valb

def compute_forward(tags_ln: int, sen_ln: int, emission_scores: List[List[float]]) -> None:
    forward_matrix = np.zeros((tags_ln, sen_ln))

    for t in range(tags_ln):
        forward_matrix[t][0] = emission_scores[t][0]

    forward = lambda tag, pos : np.logaddexp.reduce(np.array([
        forward_matrix[prev_t][pos - 1] + emission_scores[tag][pos] for prev_t in range(tags_ln)]))
    for i in range(1, sen_ln):
        for t in range(tags_ln):
            forward_matrix[t, i] = forward(t, i)
    return forward_matrix


def forward_backward(
    tag_indexer: 
    Indexer, 
    feature_indexer: Indexer, 
    sentence: LabeledSentence, 
    features: List[List[np.ndarray]],
    optimizer: Optimizer
) -> (Counter, float):
    """
    forward backward algorithm
    """
    scorer = FeatureBasedSequenceScorer(tag_indexer, features, optimizer)
            
    sen_ln = len(sentence.tokens)
    tags_ln = len(tag_indexer)
 
    emission_scores = [[scorer.score_emission(sentence.tokens, tag, pos) for pos in range(sen_ln)]
                       for tag in range(tags_ln)]
    
    forward_matrix = compute_forward(tags_ln, sen_ln, emission_scores)
  
    backward_matrix = np.zeros((tags_ln, sen_ln))
    backward = lambda tag, pos : np.logaddexp.reduce(np.array([
        backward_matrix[next_t][pos + 1] + emission_scores[next_t][pos + 1] for next_t in range(tags_ln)]))
 
    for t in range(tags_ln):
        backward_matrix[t, -1] = 0.0

    for i in range(sen_ln - 2, -1, -1):
        for t in range(tags_ln):
            backward_matrix[t, i] = backward(t, i)

    vals = [np.array([forward_matrix[tag, i] + backward_matrix[tag, i] for tag in range(tags_ln)])
            for i in range(sen_ln)]
    ret_matrix = [[np.exp(val[t] - np.logaddexp.reduce(val)) for t in range(tags_ln)] for val in vals]

    gold_label = sentence.get_bio_tags()
    assert(len(gold_label) == sen_ln)
    gold_tag_indices = [tag_indexer.index_of(gold_label[i]) for i in range(len(gold_label))]
    gold_log_prob = np.sum([emission_scores[gold_tag_indices[i]][i] 
                            for i in range(len(gold_label))])
    gradient = Counter()
    for word_idx in range(sen_ln):
        feat_ids = scorer.features[word_idx][gold_tag_indices[word_idx]]
        modify_counter_vector(gradient, feat_ids, 1.0)
        for tag_idx in range(tags_ln):
            feat_ids = scorer.features[word_idx][tag_idx]
            modify_counter_vector(gradient, feat_ids, (-1.0 * ret_matrix[word_idx][tag_idx]))

    return (gradient, gold_log_prob)

def eval(tag_indexer: Indexer, feature_indexer: Indexer, optimizer: Optimizer, args=None) -> None:
    if args is not None and hasattr(args, "eval_each_epoch") and args.eval_each_epoch:
        eval_model = CrfNerModel(tag_indexer, feature_indexer, optimizer)
        print("Running on test")
        test = read_data(args.dev_path)
        test_out = [eval_model.decode(test_ex.tokens) for test_ex in test]
        print_evaluation(test, test_out)


def train_crf_model(sentences, args=None):
    """
    Train a CRF model
    """
    import sys
    tag_indexer = Indexer()
    for sentence in sentences:
        for tag in sentence.get_bio_tags():
            tag_indexer.add_and_get_index(tag)
    print("Extracting features")
    feature_indexer = Indexer()
    # 4-d list indexed by sentence index, word index, tag index, feature index
    feature_cache = [[[[] for k in range(0, len(tag_indexer))] for j in range(0, len(sentences[i]))] for i in range(0, len(sentences))]
    for sentence_idx in range(0, len(sentences)):
        if sentence_idx % 100 == 0:
            sys.stdout.write("Ex %i/%i\r" % (sentence_idx, len(sentences)))
        for word_idx in range(0, len(sentences[sentence_idx])):
            for tag_idx in range(0, len(tag_indexer)):
                feature_cache[sentence_idx][word_idx][tag_idx] = extract_emission_features(
                    sentences[sentence_idx].tokens, word_idx, tag_indexer.get_object(tag_idx), feature_indexer, add_to_indexer=True)
    print("\nTraining")
    learning_rate = 1.0
    if args is not None and hasattr(args, "learning_rate"):
        learning_rate = args.learning_rate
    optimizer = UnregularizedAdagradTrainer(np.zeros((len(feature_indexer))), eta=learning_rate)
    num_epochs = 3
    import random
    random.seed(0)
    indices = [i for i in range(0, len(sentences))]
    import time
    
    for epoch in range(0, num_epochs):
        epoch_start = time.time()
        print("Epoch %i" % epoch)
        random.shuffle(indices)
        total_log_prob = 0.0
        for counter, i in enumerate(indices):
            if counter % 100 == 0:
                sys.stdout.write("Train %i/%i\r" % (counter, len(sentences)))
            (gradient, log_prob) = forward_backward(
                tag_indexer, feature_indexer, sentences[i], feature_cache[i], optimizer)
            total_log_prob += log_prob
            optimizer.apply_gradient_update(gradient, 1)
        print("Total log prob: %.2f time: %.2f s" % (total_log_prob, time.time() - epoch_start))
        eval(tag_indexer, feature_indexer, optimizer, args)
    use_beam = False
    beam_size = 2
    if args is not None and hasattr(args, "use_beam") and hasattr(args, "beam_size"):
        use_beam = args.use_beam
        beam_size = args.beam_size
    crf_model = CrfNerModel(tag_indexer, feature_indexer, optimizer, use_beam, beam_size)
    return crf_model    


def extract_emission_features(
    sentence_tokens: List[Token], word_index: int, tag: str, feature_indexer: Indexer, add_to_indexer: bool
) -> np.ndarray:
    """
    Extracts emission features for tagging the word at word_index with tag.
    :param sentence_tokens: sentence to extract over
    :param word_index: word index to consider
    :param tag: the tag that we're featurizing for
    :param feature_indexer: Indexer over features
    :param add_to_indexer: boolean variable indicating whether we should be expanding the indexer or not. This should
    be True at train time (since we want to learn weights for all features) and False at test time (to avoid creating
    any features we don't have weights for).
    :return: an ndarray
    """
    feats = []
    curr_word = sentence_tokens[word_index].word
    # Lexical and POS features on this word, the previous, and the next (Word-1, Word0, Word1)
    for idx_offset in range(-1, 2):
        if word_index + idx_offset < 0:
            active_word = "<s>"
        elif word_index + idx_offset >= len(sentence_tokens):
            active_word = "</s>"
        else:
            active_word = sentence_tokens[word_index + idx_offset].word
        if word_index + idx_offset < 0:
            active_pos = "<S>"
        elif word_index + idx_offset >= len(sentence_tokens):
            active_pos = "</S>"
        else:
            active_pos = sentence_tokens[word_index + idx_offset].pos
        maybe_add_feature(feats, feature_indexer, add_to_indexer, tag + ":Word" + repr(idx_offset) + "=" + active_word)
        maybe_add_feature(feats, feature_indexer, add_to_indexer, tag + ":Pos" + repr(idx_offset) + "=" + active_pos)
    # Character n-grams of the current word
    max_ngram_size = 3
    for ngram_size in range(1, max_ngram_size+1):
        start_ngram = curr_word[0:min(ngram_size, len(curr_word))]
        maybe_add_feature(feats, feature_indexer, add_to_indexer, tag + ":StartNgram=" + start_ngram)
        end_ngram = curr_word[max(0, len(curr_word) - ngram_size):]
        maybe_add_feature(feats, feature_indexer, add_to_indexer, tag + ":EndNgram=" + end_ngram)
    # Look at a few word shape features
    maybe_add_feature(feats, feature_indexer, add_to_indexer, tag + ":IsCap=" + repr(curr_word[0].isupper()))
    # Compute word shape
    new_word = []
    for i in range(0, len(curr_word)):
        if curr_word[i].isupper():
            new_word += "X"
        elif curr_word[i].islower():
            new_word += "x"
        elif curr_word[i].isdigit():
            new_word += "0"
        else:
            new_word += "?"
    maybe_add_feature(feats, feature_indexer, add_to_indexer, tag + ":WordShape=" + repr(new_word))
    return np.asarray(feats, dtype=int)

