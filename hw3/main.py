import argparse
import random
import numpy as np
import time
import torch
from torch import optim
from lf_evaluator import *
from models import *
from data import *
from utils import *
from typing import List

def _parse_args():
    """
    Command-line arguments to the system. --model switches between the main modes you'll need to use. The other arguments
    are provided for convenience.
    :return: the parsed args bundle
    """
    parser = argparse.ArgumentParser(description='main.py')
    
    # General system running and configuration options
    parser.add_argument('--do_nearest_neighbor', dest='do_nearest_neighbor', default=False, action='store_true', help='run the nearest neighbor model')

    parser.add_argument('--train_path', type=str, default='data/geo_train.tsv', help='path to train data')
    parser.add_argument('--dev_path', type=str, default='data/geo_dev.tsv', help='path to dev data')
    parser.add_argument('--test_path', type=str, default='data/geo_test.tsv', help='path to blind test data')
    parser.add_argument('--test_output_path', type=str, default='geo_test_output.tsv', help='path to write blind test results')
    parser.add_argument('--domain', type=str, default='geo', help='domain (geo for geoquery)')
    
    # Some common arguments for your convenience
    parser.add_argument('--seed', type=int, default=0, help='RNG seed (default = 0)')
    parser.add_argument('--epochs', type=int, default=100, help='num epochs to train for')
    parser.add_argument('--lr', type=float, default=.001)
    parser.add_argument('--batch_size', type=int, default=2, help='batch size')
    # 65 is all you need for GeoQuery
    parser.add_argument('--decoder_len_limit', type=int, default=65, help='output length limit of the decoder')
    parser.add_argument('--use_attention', dest='use_atten', default=False, action='store_true')
    parser.add_argument('--eval_during_train', dest='eval_during_train', default=False, action='store_true')
    # Feel free to add other hyperparameters for your input dimension, etc. to control your network
    # 50-200 might be a good range to start with for embedding and LSTM sizes
    args = parser.parse_args()
    return args


class NearestNeighborSemanticParser(object):
    """
    Semantic parser that uses Jaccard similarity to find the most similar input example to a particular question and
    returns the associated logical form.
    """
    def __init__(self, training_data: List[Example]):
        self.training_data = training_data

    def decode(self, test_data: List[Example]) -> List[List[Derivation]]:
        """
        :param test_data: List[Example] to decode
        :return: A list of k-best lists of Derivations. A Derivation consists of the underlying Example, a probability,
        and a tokenized input string. If you're just doing one-best decoding of example ex and you
        produce output y_tok, you can just return the k-best list [Derivation(ex, 1.0, y_tok)]
        """
        test_derivs = []
        for test_ex in test_data:
            test_words = test_ex.x_tok
            best_jaccard = -1
            best_train_ex = None
            # Find the highest word overlap with the train data
            for train_ex in self.training_data:
                # Compute word overlap
                train_words = train_ex.x_tok
                overlap = len(frozenset(train_words) & frozenset(test_words))
                jaccard = overlap/float(len(frozenset(train_words) | frozenset(test_words)))
                if jaccard > best_jaccard:
                    best_jaccard = jaccard
                    best_train_ex = train_ex
            # N.B. a list!
            test_derivs.append([Derivation(test_ex, 1.0, best_train_ex.y_tok)])
        return test_derivs


class Seq2SeqSemanticParser(nn.Module):
    def __init__(self, input_indexer, output_indexer, input_max_len,
                 output_max_len, hidden_size, use_atten=True, embedding_dropout=0.2, bidirect=False):
        super(Seq2SeqSemanticParser, self).__init__()
        self.input_indexer = input_indexer
        self.input_max_len = input_max_len
        self.output_indexer = output_indexer
        self.output_max_len = output_max_len
        self.hidden_size = hidden_size

        # For simplicity, use hidden_size as the embedding dimensions
        self.input_emb = EmbeddingLayer(hidden_size, len(input_indexer), embedding_dropout)
        self.output_emb = EmbeddingLayer(hidden_size, len(output_indexer), embedding_dropout)

        self.encoder = RNNEncoder(hidden_size, hidden_size, bidirect)
        self.encodeout_W = nn.Linear(hidden_size, hidden_size)
        self.dropout = nn.Dropout(0.2)
        # Wgenral is for attention only
        self.Wgeneral = nn.Linear(hidden_size, hidden_size)
        self.lstm = nn.LSTM(hidden_size, hidden_size, num_layers=1, batch_first=True,
                            dropout=0., bidirectional=bidirect)
       
        self.use_atten = use_atten
        # input is 2 * hidden_size for attention or hidden_size if not using attention
        self.W = nn.Linear(hidden_size * 2 if use_atten else hidden_size, len(output_indexer)) 
        
    def pre_log_mask(self, inp_tensor, mask):
        """
        Based on https://github.com/allenai/allennlp/blob/master/allennlp/nn/util.py#L290
        See also https://github.com/allenai/allennlp/blob/39c40fe38cd2fd36b3465b0b3c031f54ec824160/allennlp/nn/util.py#L2010
        Apply inp mask for tensor
        :param inp_tensor: a tensor
        :param mask: a tensor with the same size as inp_tensor, 1 for item, 0 for padding
        :return: a tensor same as inp_tensor except the places at the 0 mask 
        are replaced to a large negative number in log space   
        """
        if inp_tensor.size() != mask.size():
            raise TypeError("mask tensor size does not match ")
        mask = (mask.float() + 1e-13).log()
        return inp_tensor + mask

    def comput_atten_hbar(self, encode_output, inp_lens_tensor, inp_mask, outputi):
        """
        Compute attention
        :param encode_output: tensor  seq * batch_len * self.hidden_size
        :param inp_lens_tensor: a list of lengths in one batch
        :param inp_mask: mask of the input batch
        :param outputi: the output of the previous state
        :return: a tensor which is the concatenation of the previous state
        and the attention score
        """
        seq_len = len(inp_mask[0])
        batch_size = len(inp_lens_tensor) 
        atten_score = self.Wgeneral(outputi).view(1, batch_size, self.hidden_size)

        # atten_score  seq * batch_len * self.hidden_size
        # Compute attention score
        atten_score = torch.mul(atten_score.repeat(seq_len, 1, 1), encode_output)
        atten_score = torch.sum(atten_score, dim=2)

        atten_score = (self.pre_log_mask(atten_score, inp_mask.transpose(0, 1)))
        # use masked based on https://github.com/allenai/allennlp/blob/master/allennlp/nn/util.py#L216

        atten_score = torch.nn.functional.softmax(atten_score, dim=0).transpose(0, 1).unsqueeze(1)

        # Compute weighted sum
        atten_score = torch.bmm(atten_score, encode_output.transpose(0, 1))
        
        # Concatenate
        hbar = torch.cat((outputi, atten_score), 2)
        
        return hbar

    def forward(self, x_tensor, inp_lens_tensor, y_tensor, out_lens_tensor, forcing_p=0.5):
        """
        Forward of the Seq2seq model 
        :param x_tensor: input tensors
        :param inp_lens_tensor: lengths of input tensors
        :param y_tensor: label tensors
        :param out_lens_tensor: lenghts of output tensors
        :param forcing_p: teacher forcing p
        :return: a tensor batch_size x output vocab size x max output length            
        """
        y_tensor = self.output_emb.forward(y_tensor)
        input_emb = self.input_emb.forward(x_tensor)
        (encode_output, inp_mask, enc_last_states) = self.encoder.forward(input_emb, inp_lens_tensor)
        h0 = enc_last_states[0].unsqueeze(0)
        c0 = enc_last_states[1].unsqueeze(0)

        input0 = torch.zeros(len(inp_lens_tensor), 1, self.hidden_size)
        h0 = self.encodeout_W(h0)
        #h0 = self.dropout(h0)  # will dropout help?
        out = []
        outputi, (hi, ci) = self.lstm(input0, (h0, c0))
        out.append(outputi if not self.use_atten else self.comput_atten_hbar(encode_output, inp_lens_tensor, inp_mask, outputi))
 
        out_mask = self.sent_lens_to_mask(out_lens_tensor, self.output_max_len)
        inp_lens = len(inp_lens_tensor)
        for i in range(0, self.output_max_len - 1):
            # Teacher forcing
            inputi = outputi if random.random() > forcing_p else y_tensor[:, i ,:].view(inp_lens, 1, self.hidden_size)
            outputi, (hi, ci) = self.lstm(inputi, (hi, ci))
            # compute hbar is use attention
            out.append(outputi if not self.use_atten else self.comput_atten_hbar(encode_output, inp_lens_tensor, inp_mask, outputi))

        out = torch.stack(out)
        # out is sequence length x batch_size x 1 x hidden_size just after torch.stack 
        out = self.W(out.squeeze(2).transpose(0, 1)).transpose(1, 2)
        y = self.pre_log_mask(out, out_mask.unsqueeze(1).repeat(1, len(self.output_indexer), 1))
        return y

    def sent_lens_to_mask(self, lens, max_length):
        return torch.from_numpy(np.asarray([[1 if j < lens.data[i].item() else 0 for j in range(0, max_length)] for i in range(0, lens.shape[0])]))


    def decode(self, test_data: List[Example]) -> List[List[Derivation]]:
        derivs = []
        for ex in test_data:
            inp = np.array([ex.x_indexed[i] if (ex.x_indexed[i] != -1) else self.input_indexer.index_of(UNK_SYMBOL)
                            for i in range(0, len(ex.x_indexed))])
            inp = torch.from_numpy(inp)[None]

            # a dummy output for passing argument to forward function
            dummy_out = torch.zeros(1, self.hidden_size).long()
            out_lens_tensor = torch.tensor([self.output_max_len])

            inp_lens_tensor = torch.tensor([len(ex.x_indexed)])
            scores = self.forward(inp, inp_lens_tensor, dummy_out, out_lens_tensor, forcing_p=0.0)

            scores = scores[0].transpose(0, 1)
            indices = torch.argmax(scores, dim=1)
             
            tokens = []
            for idx in indices:
                # decode tokens stop if meet EOS, PAD, or None
                tok = self.output_indexer.get_object(idx.item())
                if tok == None or tok == EOS_SYMBOL or tok == "<PAD>":
                    break
                tokens.append(tok)
            derivs.append([Derivation(ex, 1.0, tokens)])

        return derivs


def make_padded_input_tensor(exs: List[Example], input_indexer: Indexer, max_len: int, reverse_input=False) -> np.ndarray:
    """
    Takes the given Examples and their input indexer and turns them into a numpy array by padding them out to max_len.
    Optionally reverses them.
    :param exs: examples to tensor-ify
    :param input_indexer: Indexer over input symbols; needed to get the index of the pad symbol
    :param max_len: max input len to use (pad/truncate to this length)
    :param reverse_input: True if we should reverse the inputs (useful if doing a unidirectional LSTM encoder)
    :return: A [num example, max_len]-size array of indices of the input tokens
    """
    if reverse_input:
        return np.array(
            [[ex.x_indexed[len(ex.x_indexed) - 1 - i] if i < len(ex.x_indexed) else input_indexer.index_of(PAD_SYMBOL)
              for i in range(0, max_len)]
             for ex in exs])
    else:
        return np.array([[ex.x_indexed[i] if i < len(ex.x_indexed) else input_indexer.index_of(PAD_SYMBOL)
                          for i in range(0, max_len)]
                         for ex in exs])

def make_padded_output_tensor(exs, output_indexer, max_len):
    """
    Similar to make_padded_input_tensor, but does it on the outputs without the option to reverse input
    :param exs:
    :param output_indexer:
    :param max_len:
    :return: A [num example, max_len]-size array of indices of the output tokens
    """
    return np.array([[ex.y_indexed[i] if i < len(ex.y_indexed) else output_indexer.index_of(PAD_SYMBOL) for i in range(0, max_len)] for ex in exs])


def encode_input_for_decoder(x_tensor, inp_lens_tensor, model_input_emb: EmbeddingLayer, model_enc: RNNEncoder):
    """
    Runs the encoder (input embedding layer and encoder as two separate modules) on a tensor of inputs x_tensor with
    inp_lens_tensor lengths.
    YOU DO NOT NEED TO USE THIS FUNCTION. It's merely meant to illustrate the usage of EmbeddingLayer and RNNEncoder
    as they're given to you, as well as show what kinds of inputs/outputs you need from your encoding phase.
    :param x_tensor: [batch size, sent len] tensor of input token indices
    :param inp_lens_tensor: [batch size] vector containing the length of each sentence in the batch
    :param model_input_emb: EmbeddingLayer
    :param model_enc: RNNEncoder
    :return: the encoder outputs (per word), the encoder context mask (matrix of 1s and 0s reflecting which words
    are real and which ones are pad tokens), and the encoder final states (h and c tuple)
    E.g., calling this with x_tensor (0 is pad token):
    [[12, 25, 0, 0],
    [1, 2, 3, 0],
    [2, 0, 0, 0]]
    inp_lens = [2, 3, 1]
    will return outputs with the following shape:
    enc_output_each_word = 3 x 4 x dim, enc_context_mask = [[1, 1, 0, 0], [1, 1, 1, 0], [1, 0, 0, 0]],
    enc_final_states = 3 x dim
    """
    input_emb = model_input_emb.forward(x_tensor)
    (enc_output_each_word, enc_context_mask, enc_final_states) = model_enc.forward(input_emb, inp_lens_tensor)
    enc_final_states_reshaped = (enc_final_states[0].unsqueeze(0), enc_final_states[1].unsqueeze(0))
    return (enc_output_each_word, enc_context_mask, enc_final_states_reshaped)


def get_len_tensor(ex_data, batch_sz=1, inp=True):
    # If inp is True get len of x_indexed
    # otherwise get len of y_indexed
    total_len = len(ex_data)
    len_arr = np.zeros((total_len //batch_sz, batch_sz))
    for (cnt, ex) in enumerate(ex_data):
        x_idx = cnt // batch_sz
        y_idx = int( cnt % batch_sz)
        if inp == True:
            len_arr[x_idx][y_idx] = len(ex.x_indexed)
        else:
            len_arr[x_idx][y_idx] = len(ex.y_indexed)
    return torch.from_numpy(len_arr).long()


def train_model_encdec(train_data: List[Example], test_data: List[Example], input_indexer, output_indexer, args) -> Seq2SeqSemanticParser:
    """
    Function to train the encoder-decoder model on the given data.
    :param train_data:
    :param test_data:
    :param input_indexer: Indexer of input symbols
    :param output_indexer: Indexer of output symbols
    :param args:
    :return: a trained Seq2SeqSemanticParser
    """
    # Sort in descending order by x_indexed, essential for pack_padded_sequence
    train_data.sort(key=lambda ex: len(ex.x_indexed), reverse=True)
    test_data.sort(key=lambda ex: len(ex.x_indexed), reverse=True)

    # Create indexed input
    input_max_len = np.max(np.asarray([len(ex.x_indexed) for ex in train_data]))
    all_train_input_data = make_padded_input_tensor(train_data, input_indexer, input_max_len, reverse_input=False)
    all_test_input_data = make_padded_input_tensor(test_data, input_indexer, input_max_len, reverse_input=False)

    output_max_len = np.max(np.asarray([len(ex.y_indexed) for ex in train_data]))
    all_train_output_data = make_padded_output_tensor(train_data, output_indexer, output_max_len)
    all_test_output_data = make_padded_output_tensor(test_data, output_indexer, output_max_len)

    print("Train length: %i" % input_max_len)
    print("Train output length: %i" % np.max(np.asarray([len(ex.y_indexed) for ex in train_data])))
    print("Train matrix: %s; shape = %s" % (all_train_input_data, all_train_input_data.shape))

    # First create a model. Then loop over epochs, loop over examples, and given some indexed words, call
    # the encoder, call your decoder, accumulate losses, update parameters
    train_items = len(all_train_input_data)

    all_train_input_tensor = torch.from_numpy(all_train_input_data)
    inp_lens_tensor = get_len_tensor(train_data, inp=True)
    all_train_output_tensor = torch.from_numpy(all_train_output_data)
    out_lens_tensor = get_len_tensor(train_data, inp=False)

    indices = [i for i in range(0, train_items)]

    hidden_size = 300
    model = Seq2SeqSemanticParser(input_indexer, output_indexer, input_max_len, output_max_len,
                                  hidden_size, args.use_atten, bidirect=False)
    loss_func = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=1e-5)

    teacher_p = 1.0
    start_t0 = time.time()

    for epoch in range(0, args.epochs):
        random.shuffle(indices)
        total_loss = 0
        t0 = time.time()
        for cnt in range(0, train_items, args.batch_size):
            batch_idx = indices[cnt: min(train_items, cnt + args.batch_size)]
            score = model.forward(all_train_input_tensor[batch_idx],
                                  inp_lens_tensor[batch_idx].squeeze(1),
                                  all_train_output_tensor[batch_idx],
                                  out_lens_tensor[batch_idx].squeeze(1),
                                  teacher_p)
            gold = all_train_output_tensor[batch_idx]
            loss = loss_func(score, gold)
            total_loss += loss

            model.zero_grad()
            loss.backward()
            optimizer.step()
            teacher_p = max(0.0, teacher_p * 0.95)
        run_time = time.time() - t0
        print("Epoch %d: total loss %.3lf in %.2lf seconds" %
              (epoch, total_loss, run_time))
        if args.eval_during_train and (epoch + 1) % 20 == 0:
            model.eval()
            evaluate(test_data, model)
            model.train()
    train_time = time.time() - start_t0
    print("Total train time: %.2lf seconds" % train_time)
    model.eval()
    return model


def evaluate(test_data: List[Example], decoder, example_freq=50, print_output=True, outfile=None):
    """
    Evaluates decoder against the data in test_data (could be dev data or test data). Prints some output
    every example_freq examples. Writes predictions to outfile if defined. Evaluation requires
    executing the model's predictions against the knowledge base. We pick the highest-scoring derivation for each
    example with a valid denotation (if you've provided more than one).
    :param test_data:
    :param decoder:
    :param example_freq: How often to print output
    :param print_output:
    :param outfile:
    :return:
    """
    e = GeoqueryDomain()
    pred_derivations = decoder.decode(test_data)
    java_crashes = False
    if java_crashes:
        selected_derivs = [derivs[0] for derivs in pred_derivations]
        denotation_correct = [False for derivs in pred_derivations]
    else:
        selected_derivs, denotation_correct = e.compare_answers([ex.y for ex in test_data], pred_derivations, quiet=True)
    print_evaluation_results(test_data, selected_derivs, denotation_correct, example_freq, print_output)
    # Writes to the output file if needed
    if outfile is not None:
        with open(outfile, "w") as out:
            for i, ex in enumerate(test_data):
                out.write(ex.x + "\t" + " ".join(selected_derivs[i].y_toks) + "\n")
        out.close()


if __name__ == '__main__':
    args = _parse_args()
    print(args)
    random.seed(args.seed)
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    # Load the training and test data

    train, dev, test = load_datasets(args.train_path, args.dev_path, args.test_path, domain=args.domain)
    train_data_indexed, dev_data_indexed, test_data_indexed, input_indexer, output_indexer = index_datasets(train, dev, test, args.decoder_len_limit)
    print("%i train exs, %i dev exs, %i input types, %i output types" % (len(train_data_indexed), len(dev_data_indexed), len(input_indexer), len(output_indexer)))
    print("Input indexer: %s" % input_indexer)
    print("Output indexer: %s" % output_indexer)
    print("Here are some examples post tokenization and indexing:")
    for i in range(0, min(len(train_data_indexed), 10)):
        print(train_data_indexed[i])
    if args.do_nearest_neighbor:
        decoder = NearestNeighborSemanticParser(train_data_indexed)
        evaluate(dev_data_indexed, decoder)
    else:
        decoder = train_model_encdec(train_data_indexed, dev_data_indexed, input_indexer, output_indexer, args)
        evaluate(dev_data_indexed, decoder)
    print("=======FINAL EVALUATION ON BLIND TEST=======")
    evaluate(test_data_indexed, decoder, print_output=False, outfile="geo_test_output.tsv")


